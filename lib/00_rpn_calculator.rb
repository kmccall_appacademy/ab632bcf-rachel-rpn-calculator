class RPNCalculator
  attr_accessor :calculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    do_math(:+)
  end

  def minus
    do_math(:-)
  end

  def times
    do_math(:*)
  end

  def divide
    do_math(:/)
  end

  def value
    @stack.last
  end

  def tokens(string)
    token = [:+, :-, :*, :/]
    result = []
    string.split.each do |el|
      if token.include?(el.to_sym)
        result << el.to_sym
      else
        result << el.to_i
      end
    end
    result
  end

  def evaluate(string)
    arr = tokens(string)
    arr.each do |el|
      if el.is_a? Integer
        @stack << el
      elsif el == :+
        self.plus
      elsif el == :-
        self.minus
      elsif el == :*
        self.times
        p self
      elsif el == :/
        self.divide
      end
    end
    @stack.last
  end

  private

  def do_math(symbol)
    if @stack.length < 2
      raise "calculator is empty"
    else
      var1 = @stack.pop
      var2 = @stack.pop
    end
    case symbol
    when :+
      @stack << (var1 + var2)
    when :-
      @stack << (var2 - var1)
    when :*
      @stack << (var2 * var1)
    when :/
      @stack << (var2.to_f / var1)
    end
    @stack
  end


end
